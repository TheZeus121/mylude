{-# LANGUAGE NoImplicitPrelude #-}

module Mylude (module X,
               ListLike(..),

               (++), append, concat, empty, intercalate) where

import Protolude as X hiding (Generic(..), Location, Type,
                              (++), concat, decodeUtf8, drop, empty,
                              encodeUtf8, genericDrop, genericLength,
                              genericSplitAt, genericTake, intercalate,
                              length, splitAt, take)

import Data.Text.Lazy.Encoding as X (decodeUtf8, decodeUtf16LE, encodeUtf8, encodeUtf16LE)

import qualified Data.ByteString.Lazy as BS (ByteString, drop, length, splitAt, take)
import qualified Data.Text.Lazy       as LT (Text, drop, length, splitAt, take)
import qualified Protolude            as P  (drop, genericDrop, genericSplitAt, genericTake,
                                             genericLength, length, splitAt, take)

append :: Monoid a => a -> a -> a
append = mappend
{-# INLINE append #-}

(++) :: Monoid a => a -> a -> a
(++) = append
{-# INLINE (++) #-}

concat :: Monoid a => [a] -> a
concat = mconcat
{-# INLINE concat #-}

empty :: Monoid a => a
empty = mempty
{-# INLINE empty #-}

intercalate :: Monoid a => a -> [a] -> a
intercalate x xs = concat $ intersperse x xs

class ListLike a where
  {-# MINIMAL length | genericLength,
              genericSplitAt | (genericDrop, genericTake) #-}
  -- | @'drop' n xs@ returns the suffix of @xs@ after the first @n@
  -- elements, or @[]@ if @n > 'length' xs@.  It is an instance of the
  -- more general 'genericDrop', in which @n@ may be of any integral
  -- type.
  drop :: Int -> a -> a
  -- | The 'genericDrop' function is an overloaded version of 'drop',
  -- which accepts any 'Integral' value as the number of elements to
  -- drop.
  genericDrop :: Integral i => i -> a -> a

  -- | Returns the size/length of a finite structure as an 'Int'.
  length :: a -> Int
  -- | The 'genericLength' function is an overloaded version of
  -- 'length'.  In particular, instead of returning an 'Int', it
  -- returns any type which is an instance of 'Num'.  It is, however,
  -- less efficient than 'length'.
  genericLength :: Num i => a -> i

  -- | @'splitAt' n xs@ returns a tuple where first element is @xs@
  -- prefix of length @n@ and second element is the remainder of the
  -- list.  It is equivalent to @('take' n xs, 'drop' n xs)@ when @n@
  -- is not @_|_@ (@splitAt _|_ xs = _|_@).  'splitAt' is an instance
  -- of the more general 'genericSplitAt', in which @n@ may be of any
  -- integral type.
  splitAt :: Int -> a -> (a, a)
  -- | The 'genericSplitAt' function is an overloaded version of
  -- 'splitAt', which accepts any 'Integral' value as the position at
  -- which to split.
  genericSplitAt :: Integral i => i -> a -> (a, a)

  -- | @'take' n@, applied to a list @xs@, returns the prefix of @xs@
  -- of length @n@, or @xs@ itself if @n > 'length' xs@.  It is an
  -- instance of the more general 'genericTake', in which @n@ may be
  -- of any integral type.
  take :: Int -> a -> a
  -- | The 'genericTake' function is an overloaded version of 'take',
  -- which accepts any 'Integral' value as the number of elements to
  -- take.
  genericTake :: Integral i => i -> a -> a

  drop = genericDrop
  genericDrop n xs = suf where (_, suf) = genericSplitAt n xs

  length = genericLength
  genericLength = fromIntegral . length

  splitAt = genericSplitAt
  genericSplitAt n xs = (pre, suf) where pre = genericTake n xs
                                         suf = genericDrop n xs

  take = genericTake
  genericTake n xs = pre where (pre, _) = genericSplitAt n xs

instance ListLike [a] where
  drop = P.drop
  genericDrop = P.genericDrop

  length = P.length
  genericLength = P.genericLength

  splitAt = P.splitAt
  genericSplitAt = P.genericSplitAt

  take = P.take
  genericTake = P.genericTake

instance ListLike LT.Text where
  genericDrop = LT.drop . fromIntegral
  genericLength = fromIntegral . LT.length
  genericSplitAt = LT.splitAt . fromIntegral
  genericTake = LT.take . fromIntegral

instance ListLike BS.ByteString where
  genericDrop = BS.drop . fromIntegral
  genericLength = fromIntegral . BS.length
  genericSplitAt = BS.splitAt . fromIntegral
  genericTake = BS.take . fromIntegral
